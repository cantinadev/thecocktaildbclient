# TheCocktailDB Go Client Library
## A Go client library for https://www.thecocktaildb.com 

## API Key Needed!
You'll need an API key from https://www.thecocktaildb.com to use this in production and run the integration tests.

It is the responsibility of the consumers of this library to adhere to the rate limits and other terms of service of The
Cocktail DB.

## Quickstart
    package main
    import (
        "fmt"
        "net/http"
        "gitlab.com/cantinadev/thecocktaildbclient/fetcher"
    )
    
    func main() {
        // "1" is a test key for evaluating the API.  Don't expect it to work with actual load
        ctdb := fetcher.New("1", &http.Client{})
        drinks, _ := ctdb.SearchByIngredients([]string{"vodka"})
        for _, d := range drinks {
            fmt.Println(d.Name)
        }

        drink, _ := ctdb.GetRandom()
        fmt.Println(drink.Instructions)

        drinks, _ = ctdb.SearchByName("Baby Guinness")
        fmt.Printf("%v\n", drinks[0].Ingredients)
        fmt.Println(drinks[0].Instructions)
    }


## Testing
`make unit` To run unit tests

`TCDB_API_KEY=<your api key> make integration` to run integration tests.