package fetcher

import "gitlab.com/cantinadev/thecocktaildbclient/cocktail"

type Fetcher interface {
	GetRandom() cocktail.Drink
	SearchByIngredients(ingredients []string) []cocktail.Drink
	SearchByName(name string) []cocktail.Drink
}
