package fetcher

import (
	"encoding/json"
	"fmt"
	"gitlab.com/cantinadev/thecocktaildbclient/cocktail"
	"io"
	"net/http"
	"net/url"
	"strings"
)

type TheCocktailDB struct {
	key string
	requester HttpRequester
}

func (c *TheCocktailDB) GetRandom() (cocktail.Drink, error) {
	response, err := c.requester.Get(c.getBaseURI() + "/random.php")
	if err != nil {
		return cocktail.Drink{}, err
	}
	defer response.Body.Close()
	drinks, err := c.parseDrink(response)
	if err != nil || len(drinks) == 0 {
		return cocktail.Drink{}, err
	}
	return drinks[0], err
}

func (c *TheCocktailDB) SearchByName(name string) ([]cocktail.Drink, error) {
	response, err := c.requester.Get(fmt.Sprintf("%s/search.php?s=%s", c.getBaseURI(), url.QueryEscape(name)))
	if err != nil {
		return []cocktail.Drink{}, err
	}
	defer response.Body.Close()
	drinks, err := c.parseDrink(response)

	if err != nil || len(drinks) == 0 {
		return []cocktail.Drink{}, err
	}

	return drinks, err
}

func (c *TheCocktailDB) SearchByIngredients(ingredients []string) ([]cocktail.Drink, error) {
	response, err := c.requester.Get(fmt.Sprintf("%s/filter.php?i=%s", c.getBaseURI(), url.QueryEscape(
		strings.Join(ingredients, ","))))
	if err != nil {
		return []cocktail.Drink{}, err
	}
	defer response.Body.Close()
	drinks, err := c.parseDrink(response)

	if err != nil || len(drinks) == 0 {
		return []cocktail.Drink{}, err
	}

	return drinks, err
}

func New(key string, requester HttpRequester) TheCocktailDB {
	return TheCocktailDB{
		key: key,
		requester: requester,
	}
}

func (c *TheCocktailDB) getBaseURI() string {
	return fmt.Sprintf("https://www.thecocktaildb.com/api/json/v2/%s", c.key)
}

func (c *TheCocktailDB) parseDrink(response *http.Response) ([]cocktail.Drink, error) {
	type rawDrinkData struct {
		IdDrink           string
		StrDrink          string
		StrDrinkAlternate *string
		StrCategory       string
		StrAlcoholic      string
		StrGlass          *string
		StrInstructions   string
		StrDrinkThumb     string
		StrIngredient1    *string
		StrIngredient2    *string
		StrIngredient3    *string
		StrIngredient4    *string
		StrIngredient5    *string
		StrIngredient6    *string
		StrIngredient7    *string
		StrIngredient8    *string
		StrIngredient9    *string
		StrIngredient10   *string
		StrIngredient11   *string
		StrIngredient12   *string
		StrIngredient13   *string
		StrIngredient14   *string
		StrIngredient15   *string
		StrMeasure1       *string
		StrMeasure2       *string
		StrMeasure3       *string
		StrMeasure4       *string
		StrMeasure5       *string
		StrMeasure6       *string
		StrMeasure7       *string
		StrMeasure8       *string
		StrMeasure9       *string
		StrMeasure10      *string
		StrMeasure11      *string
		StrMeasure12      *string
		StrMeasure13      *string
		StrMeasure14      *string
		StrMeasure15      *string
	}

	rawDrinkList := struct {
		Drinks []rawDrinkData
	}{}

	err := json.NewDecoder(response.Body).Decode(&rawDrinkList)
	if err != nil {
		if err == io.EOF {
			return []cocktail.Drink{}, nil
		} else {
			return nil, err
		}
	}

	var drinks []cocktail.Drink
	for _, d := range rawDrinkList.Drinks {
		var ingredients []cocktail.Ingredient
		parsedDrink := cocktail.Drink {
			Id:            d.IdDrink,
			Name:          d.StrDrink,
			AlternateName: d.StrDrinkAlternate,
			Category:      d.StrCategory,
			Glass:         d.StrGlass,
			Instructions:  d.StrInstructions,
			Image:         d.StrDrinkThumb,
			Ingredients:   []cocktail.Ingredient{},
		}

		// God this is pain.  But I can't help it that this API uses the stupidest data format ever.
		if d.StrIngredient1 != nil && d.StrMeasure1 != nil {
			ingredients = append(ingredients, cocktail.Ingredient{
				Name:   *d.StrIngredient1,
				Amount: *d.StrMeasure1,
			})
		}
		if d.StrIngredient2 != nil && d.StrMeasure2 != nil {
			ingredients = append(ingredients, cocktail.Ingredient{
				Name:   *d.StrIngredient2,
				Amount: *d.StrMeasure2,
			})
		}
		if d.StrIngredient3 != nil && d.StrMeasure3 != nil {
			ingredients = append(ingredients, cocktail.Ingredient{
				Name:   *d.StrIngredient3,
				Amount: *d.StrMeasure3,
			})
		}
		if d.StrIngredient4 != nil && d.StrMeasure4 != nil {
			ingredients = append(ingredients, cocktail.Ingredient{
				Name:   *d.StrIngredient4,
				Amount: *d.StrMeasure4,
			})
		}
		if d.StrIngredient5 != nil && d.StrMeasure5 != nil {
			ingredients = append(ingredients, cocktail.Ingredient{
				Name:   *d.StrIngredient5,
				Amount: *d.StrMeasure5,
			})
		}
		if d.StrIngredient6 != nil && d.StrMeasure6 != nil {
			ingredients = append(ingredients, cocktail.Ingredient{
				Name:   *d.StrIngredient6,
				Amount: *d.StrMeasure6,
			})
		}
		if d.StrIngredient7 != nil && d.StrMeasure7 != nil {
			ingredients = append(ingredients, cocktail.Ingredient{
				Name:   *d.StrIngredient7,
				Amount: *d.StrMeasure7,
			})
		}
		if d.StrIngredient8 != nil && d.StrMeasure8 != nil {
			ingredients = append(ingredients, cocktail.Ingredient{
				Name:   *d.StrIngredient8,
				Amount: *d.StrMeasure8,
			})
		}
		if d.StrIngredient9 != nil && d.StrMeasure9 != nil {
			ingredients = append(ingredients, cocktail.Ingredient{
				Name:   *d.StrIngredient9,
				Amount: *d.StrMeasure9,
			})
		}
		if d.StrIngredient10 != nil && d.StrMeasure10 != nil {
			ingredients = append(ingredients, cocktail.Ingredient{
				Name:   *d.StrIngredient10,
				Amount: *d.StrMeasure10,
			})
		}
		if d.StrIngredient11 != nil && d.StrMeasure11 != nil {
			ingredients = append(ingredients, cocktail.Ingredient{
				Name:   *d.StrIngredient11,
				Amount: *d.StrMeasure11,
			})
		}
		if d.StrIngredient12 != nil && d.StrMeasure12 != nil {
			ingredients = append(ingredients, cocktail.Ingredient{
				Name:   *d.StrIngredient12,
				Amount: *d.StrMeasure12,
			})
		}
		if d.StrIngredient13 != nil && d.StrMeasure13 != nil {
			ingredients = append(ingredients, cocktail.Ingredient{
				Name:   *d.StrIngredient13,
				Amount: *d.StrMeasure13,
			})
		}
		if d.StrIngredient14 != nil && d.StrMeasure14 != nil {
			ingredients = append(ingredients, cocktail.Ingredient{
				Name:   *d.StrIngredient14,
				Amount: *d.StrMeasure14,
			})
		}
		if d.StrIngredient15 != nil && d.StrMeasure15 != nil {
			ingredients = append(ingredients, cocktail.Ingredient{
				Name:   *d.StrIngredient15,
				Amount: *d.StrMeasure15,
			})
		}

		parsedDrink.Ingredients = ingredients
		drinks = append(drinks, parsedDrink)
	}

	return drinks, nil
}
