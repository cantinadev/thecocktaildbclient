// +build integration

package fetcher

import (
	"github.com/dnaeon/go-vcr/v2/recorder"
	"github.com/stretchr/testify/assert"
	"log"
	"net/http"
	"os"
	"testing"
)

var apiKey = os.Getenv("TCDB_API_KEY")

func TestTheCocktailDB_GetRandomIntegration(t *testing.T) {
	client := &http.Client{}
	rec, err := recorder.New("fixtures/tcdb-random")
	client.Transport = rec
	db := New(apiKey, client)

	if err != nil {
		log.Fatal(err)
	}
	defer rec.Stop()
	drink, err := db.GetRandom()
	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, "Amaretto Shake", drink.Name)
}

func TestTheCocktailDB_SearchByNameIntegrationTest(t *testing.T) {
	client := &http.Client{}
	rec, err := recorder.New("fixtures/tcdb-byname")
	client.Transport = rec
	db := New(apiKey, client)

	if err != nil {
		log.Fatal(err)
	}
	defer rec.Stop()
	drink, err := db.SearchByName("Margarita")
	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, "Margarita", drink[0].Name)
}

func TestTheCocktailDB_SearchByIngredientsIntegrationTest(t *testing.T) {
	client := &http.Client{}
	rec, err := recorder.New("fixtures/tcdb-byingredients")
	client.Transport = rec
	db := New(apiKey, client)

	if err != nil {
		log.Fatal(err)
	}
	defer rec.Stop()
	drink, err := db.SearchByIngredients([]string{"everclear"})
	if err != nil {
		t.Error(err)
	}
	assert.Len(t, drink, 4)
}
