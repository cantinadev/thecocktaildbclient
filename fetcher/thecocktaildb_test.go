package fetcher

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"io"
	"net/http"
	"testing"
)

var baseUrl = "https://www.thecocktaildb.com/api/json/v2/"
var whiteRussianBytes = []byte("{\"drinks\":[{\"idDrink\":\"12528\",\"strDrink\":\"White Russian\",\"strDrinkAlternate\":null,\"strTags\":null,\"strVideo\":null,\"strCategory\":\"Ordinary Drink\",\"strIBA\":null,\"strAlcoholic\":\"Alcoholic\",\"strGlass\":\"Old-fashioned glass\",\"strInstructions\":\"Pour vodka and coffee liqueur over ice cubes in an old-fashioned glass. Fill with light cream and serve.\",\"strInstructionsES\":null,\"strInstructionsDE\":\"Gie\\u00dfen Sie Wodka und Kaffeelik\\u00f6r \\u00fcber Eisw\\u00fcrfel in einem old-fashioned Glas. Mit Sahne auff\\u00fcllen und servieren.\",\"strInstructionsFR\":null,\"strInstructionsIT\":\"Versare la vodka e il liquore al caff\\u00e8 sui cubetti di ghiaccio in un bicchiere vecchio stile.Farcite con crema leggera e servite.\",\"strInstructionsZH-HANS\":null,\"strInstructionsZH-HANT\":null,\"strDrinkThumb\":\"https:\\/\\/www.thecocktaildb.com\\/images\\/media\\/drink\\/vsrupw1472405732.jpg\",\"strIngredient1\":\"Vodka\",\"strIngredient2\":\"Coffee liqueur\",\"strIngredient3\":\"Light cream\",\"strIngredient4\":null,\"strIngredient5\":null,\"strIngredient6\":null,\"strIngredient7\":null,\"strIngredient8\":null,\"strIngredient9\":null,\"strIngredient10\":null,\"strIngredient11\":null,\"strIngredient12\":null,\"strIngredient13\":null,\"strIngredient14\":null,\"strIngredient15\":null,\"strMeasure1\":\"2 oz \",\"strMeasure2\":\"1 oz \",\"strMeasure3\":null,\"strMeasure4\":null,\"strMeasure5\":null,\"strMeasure6\":null,\"strMeasure7\":null,\"strMeasure8\":null,\"strMeasure9\":null,\"strMeasure10\":null,\"strMeasure11\":null,\"strMeasure12\":null,\"strMeasure13\":null,\"strMeasure14\":null,\"strMeasure15\":null,\"strImageSource\":null,\"strImageAttribution\":null,\"strCreativeCommonsConfirmed\":\"No\",\"dateModified\":\"2016-08-28 18:35:32\"}]}")
type mockHttpRequester struct {
	mock.Mock
}

func (m *mockHttpRequester) Get(uri string) (*http.Response, error) {
	args := m.Called(uri)
	return args.Get(0).(*http.Response), args.Error(1)
}

func TestTheCocktailDB_GetRandom(t *testing.T) {
	mockRequester := &mockHttpRequester{}
	db := TheCocktailDB{
		key:       "testKey",
		requester: mockRequester,
	}
	var whiteRussianReader = bytes.NewReader(whiteRussianBytes)
	var whiteRussianReturn = http.Response{
		Status:           "OK",
		StatusCode:       200,
		Body:             io.NopCloser(whiteRussianReader),
	}

	mockRequester.On("Get", baseUrl + "testKey/random.php").Return(&whiteRussianReturn, nil)
	drink, _ := db.GetRandom()
	mockRequester.AssertExpectations(t)
	assert.Equal(t, "White Russian", drink.Name)
}

func TestTheCocktailDB_SearchByName(t *testing.T) {
	mockRequester := &mockHttpRequester{}
	db := TheCocktailDB{
		key:       "testKey",
		requester: mockRequester,
	}

	var whiteRussianReader = bytes.NewReader(whiteRussianBytes)
	var whiteRussianReturn = http.Response{
		Status:           "OK",
		StatusCode:       200,
		Body:             io.NopCloser(whiteRussianReader),
	}

	mockRequester.On("Get", baseUrl + "testKey/search.php?s=White+Russian").Return(&whiteRussianReturn, nil)
	drink, _ := db.SearchByName("White Russian")
	mockRequester.AssertExpectations(t)
	assert.Equal(t, "White Russian", drink[0].Name)
}

func TestTheCocktailDB_SearchByIngredients(t *testing.T) {
	mockRequester := &mockHttpRequester{}
	db := TheCocktailDB{
		key:       "testKey",
		requester: mockRequester,
	}

	var whiteRussianReader = bytes.NewReader(whiteRussianBytes)
	var whiteRussianReturn = http.Response{
		Status:           "OK",
		StatusCode:       200,
		Body:             io.NopCloser(whiteRussianReader),
	}

	mockRequester.On("Get", baseUrl + "testKey/filter.php?i=vodka").Return(&whiteRussianReturn, nil)
	drink, _ := db.SearchByIngredients([]string{"vodka"})
	mockRequester.AssertExpectations(t)
	assert.Equal(t, "White Russian", drink[0].Name)
}