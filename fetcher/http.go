package fetcher

import "net/http"

type HttpRequester interface {
	Get(uri string) (*http.Response, error)
}
