module gitlab.com/cantinadev/thecocktaildbclient

go 1.16

require (
	github.com/dnaeon/go-vcr/v2 v2.0.1 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
)
