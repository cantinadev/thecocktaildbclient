package cocktail

type Drink struct {
	Id string
	Name string
	AlternateName *string
	Category string
	Glass *string
	Instructions string
	Image string
	Ingredients []Ingredient
}

type Ingredient struct {
	Name string
	Amount string
}
